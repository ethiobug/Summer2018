Hello  

Here you will find the materials from the tutorials run during the summer of 2018. The files are organized according to the activity (and day). 


For the molecular epidemiology activity on day 3:

There are instruction files (in both .txt and .pdf format) for how to setup your computing environment and how to do the activity. All data files you need are in the mol_epi_files.zip file. Download this file and unzip it on your computer to get a folder containing all the data files you need for the full activity. Files you generate during the activity are also in this foler and may be overwritten as you progress through the activity.


For the microbiome activity on day 4: 

The Mothur_data directory has the instructions for both Windows and Linux along with the sequences files needed. Unfortunately, due to upload size restraints, the main database sequence file could not be uploaded right now. I am working on this issue and will update here when I figure out a work-around. 
In order for the Mothur tutorial to run properly, all of the files need to be in the correct file structure. You will need to make sure this is the case on your own computer after downloading the files you need. No files should have ".gz" at the end except for the ones in the data/ directory, so you will need to gunzip those that do. \
The file called 'File_structure' is how it should be set up when you start.  

The final.database file is an example of what you should end up with when you finish (or something similar).  \
The file that is missing right now is the silva.bacteria.fasta.
